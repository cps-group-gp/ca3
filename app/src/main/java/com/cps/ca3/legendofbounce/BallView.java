package com.cps.ca3.legendofbounce;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

public class BallView extends View {

    private Ball ball;

    public BallView(Context context, int color, int displayHeight, int displayWidth) {
        super(context);
        ball = new Ball(color);
        ball.setRandomInitials(displayHeight, displayWidth);
    }

    public void moveBall(double angleX, double angleY, double angleZ, float displayHeight, float displayWidth){
        ball.move(angleX, angleY, angleZ, displayHeight, displayWidth);
    }

    public void moveByGravity(double gX, double gY, float displayHeight, float displayWidth){
        ball.moveByGravity(gX, gY, displayHeight, displayWidth);
    }

    public void jump(){
        ball.jump();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        ball.draw(canvas);
    }

}