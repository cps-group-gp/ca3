package com.cps.ca3.legendofbounce;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;

import java.util.*;
import android.os.Trace;

public class Ball {
    float radius = 80;
    float x = radius + 20;  // Ball's center (x,y)
    float y = radius + 40;
    float speedX;       // Ball's speed (x,y)
    float speedY;
    float aX;
    float aY;
    double uK = 0.07;
    double uS = 0.15;
    private RectF bounds;   // Needed for Canvas.drawOval
    private Paint paint;    // The paint style, color used for drawing

    private int borderLength = 40;

    private float interval = 0.1f;

    private static final float G = 10.0f;

    private float velocityReduction = (float) Math.sqrt(1 - GameActivity.K_ENERGY_REDUCTION_PERCENTAGE);

    public Ball(int color) {
        bounds = new RectF();
        paint = new Paint();
        paint.setColor(color);
    }

    public void draw(Canvas canvas) {
        Trace.beginSection("begin drawing ball");

        bounds.set(x-radius, y-radius, x+radius, y+radius);
        canvas.drawOval(bounds, paint);

        Trace.endSection();
    }

    public void setRandomInitials(float displayHeight, float displayWidth) {
        x = (float) ((Math.random() * displayWidth));
        y = (float) ((Math.random() * displayHeight));
        speedX = (float) Math.random() * 10;
        speedY = (float) Math.random() * 10;
        aX = 0;
        aY = 0;
    }

    public void move(double angleX, double angleY, double angleZ, float displayHeight, float displayWidth) {
        setAcceleration(angleX, angleY, angleZ, displayHeight, displayWidth);
        updateCoordinationAndSpeed(displayHeight, displayWidth);
    }

    public void updateCoordinationAndSpeed(float displayHeight, float displayWidth){
        x += ((0.5 * aX) * interval * interval + speedX * interval) * 100;
        y += ((0.5 * aY) * interval * interval + speedY * interval) * 100;

        displayHeight -= borderLength;
        displayWidth -= borderLength;

        // Detect collision and react
        if (x + radius > displayWidth) {
            speedX = -speedX * velocityReduction;
            x = displayWidth - radius;
        }
        else if (x - radius < borderLength) {
            speedX = -speedX * velocityReduction;
            x = radius + borderLength;
        }
        if (y + radius > displayHeight) {
            speedY = -speedY * velocityReduction;
            y = displayHeight - radius;
        }
        else if (y - radius < borderLength) {
            speedY = -speedY * velocityReduction;
            y = radius + borderLength;
        }
        speedX += aX * interval;
        speedY += aY * interval;
    }

    public void setAcceleration(double angleX, double angleY, double angleZ, float displayHeight, float displayWidth){

        aY = (float)( G * Math.cos(Math.toRadians(angleZ)));
        aX = - (float)( G * Math.sin(Math.toRadians(angleZ)));

        setAccelerationOnFloors(angleZ, displayHeight, displayWidth);
    }

    public void setAccelerationOnFloors(double angleZ, float displayHeight, float displayWidth){
        displayHeight -= borderLength;
        displayWidth -= borderLength;
        if(angleZ < 90){
            if(y - radius <= borderLength)
                setAccelerationOnYFloors(angleZ);
            else if(x + radius >= displayHeight)
                setAccelerationOnXFloors(angleZ);
        }
        else if(angleZ < 180){
            if(x + radius >= displayHeight)
                setAccelerationOnXFloors(angleZ);
            else if(y + radius >= displayWidth)
                setAccelerationOnYFloors(angleZ);
        }
        else if(angleZ < 270){
            if(y + radius >= displayWidth)
                setAccelerationOnYFloors(angleZ);
            else if(x - radius <= borderLength)
                setAccelerationOnXFloors(angleZ);
        }
        else{
            if(x - radius <= borderLength)
                setAccelerationOnXFloors(angleZ);
            else if(y - radius <= borderLength)
                setAccelerationOnYFloors(angleZ);
        }
    }

    public void setAccelerationOnYFloors(double angleZ){
        aY = 0;

        if(angleZ < 90){
            double angleRadian = Math.toRadians(angleZ);
            if (speedY == 0 && speedX > 0){
                aX = - (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX < 0){
                aX = - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX == 0){
                aX = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aX = 0;
        }
        else if(angleZ < 180){
            double angleRadian = Math.toRadians(180 - angleZ);
            if (speedY == 0 && speedX > 0){
                aX = - (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX < 0){
                aX = - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX == 0){
                aX = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aX = 0;
        }
        else if(angleZ < 270){
            double angleRadian = Math.toRadians(angleZ - 180);
            if (speedY == 0 && speedX > 0){
                aX = (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX < 0){
                aX = (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX == 0){
                aX = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                       (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aX = 0;
        }
        else{
            double angleRadian = Math.toRadians(360 - angleZ);
            if (speedY == 0 && speedX > 0){
                aX = (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX < 0){
                aX = (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedY == 0 && speedX == 0){
                aX = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aX = 0;
        }

    }

    public void setAccelerationOnXFloors(double angleZ){
        aX = 0;
        if(angleZ < 90){
            double angleRadian = Math.toRadians(90 - angleZ);
            if(speedX == 0 && speedY > 0)
                aY = (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            else if(speedX == 0 && speedY < 0)
                aY = (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            else if(speedX == 0 && speedY == 0)
                aY = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            else
                aY = 0;
        }
        else if(angleZ < 180){
            double angleRadian = Math.toRadians(angleZ - 90);
            if(speedX == 0 && speedY > 0)
                aY = - (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            else if(speedX == 0 && speedY < 0)
                aY = - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            else if(speedX == 0 && speedY == 0)
                aY = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            else
                aY = 0;
        }
        else if(angleZ < 270){
            double angleRadian = Math.toRadians(270 - angleZ);
            if(speedX == 0 && speedY > 0){
                aY = - (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedX == 0 && speedY < 0){
                aY = - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedX == 0 && speedY == 0){
                aY = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        - (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aY = 0;
        }
        else{
            double angleRadian = Math.toRadians(angleZ - 270);
            if(speedX == 0 && speedY > 0){
                aY = (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK);
            }
            else if(speedX == 0 && speedY < 0){
                aY = (float)(G * Math.sin(angleRadian) + G * Math.cos(angleRadian) * uK);
            }
            else if(speedX == 0 && speedY == 0){
                aY = G * Math.sin(angleRadian) > G * Math.cos(angleRadian) * uS ?
                        (float)(G * Math.sin(angleRadian) - G * Math.cos(angleRadian) * uK) : 0;
            }
            else
                aY = 0;
        }
    }

    public void moveByGravity(double gX, double gY, float displayHeight, float displayWidth){
        double angleZ = Math.toDegrees(Math.atan2(-gY, gX));
        angleZ = angleZ < 0 ? 360 + angleZ : angleZ;
        aX = (float) gY;
        aY = (float) gX;
        setAccelerationOnFloors(angleZ, displayHeight, displayWidth);
        Log.d("MyActivity", "In Ball: " + gX+ ", " + gY + " , Angle: " + angleZ);
        updateCoordinationAndSpeed(displayHeight, displayWidth);
    }

    public void jump(){
        speedX = (float) Math.random() * 10;
        speedY = (float) Math.random() * 10;
    }

}
