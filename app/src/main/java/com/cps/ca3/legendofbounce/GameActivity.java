package com.cps.ca3.legendofbounce;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.os.Trace;

public class GameActivity extends AppCompatActivity implements
        View.OnClickListener {

    private static final String TAG = "MyActivity";

    private static final float RADIANS_TO_DEGREES = (float) (180d / Math.PI);
    private static final float G = (float)10.0;

    public static final double K_ENERGY_REDUCTION_PERCENTAGE = 0.1;

    private double currentAngleX = 0;
    private double currentAngleY = 0;
    private double currentAngleZ = 0;

    private double gX = G;
    private double gY = 0;

    Handler handler = new Handler();
    Runnable runnable;
    int interval = 100;

    int mode = -1;
    BallView ballView;
    int height, width;

    private SensorManager sensorManager;
    private Sensor sensorGyroscope;
    private Sensor sensorGravity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        Bundle bMode = getIntent().getExtras();
        if (bMode != null)
            mode = bMode.getInt("mode");
        if (mode == 0)
            Toast.makeText(this, "Gyroscope Mode", Toast.LENGTH_LONG).show();
        else
            Toast.makeText(this, "Gravity Mode", Toast.LENGTH_LONG).show();

        Button modeButton = (Button) findViewById(R.id.button1);
        modeButton.setOnClickListener(this); // calling onClick() method

        Button jumpButton = (Button) findViewById(R.id.button2);
        jumpButton.setOnClickListener(this); // calling onClick() method

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;

        ballView = new BallView(this, Color.BLUE, height, width);
        LinearLayout layout1 = (LinearLayout) findViewById(R.id.drawBall);
        layout1.addView(ballView);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorGyroscope = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        sensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);

        if (sensorGyroscope == null) {
            Toast.makeText(this, "This device does not have a gyroscope sensor!", Toast.LENGTH_LONG).show();
        }
        if (sensorGravity == null) {
            Toast.makeText(this, "This device does not have a gravity sensor!", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                if (mode == 0) {
                    mode = 1;
                    Toast.makeText(this, "Gravity Mode", Toast.LENGTH_SHORT).show();
                }
                else {
                    mode = 0;
                    Toast.makeText(this, "Gyroscope Mode", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.button2:
                ballView.jump();
                break;
            default:
                break;
        }
    }

    private SensorEventListener gyroscopeListener = new SensorEventListener() {

        private static final float MIN_TIME_STEP = (1f / 40f);
        private long lastTime = System.currentTimeMillis();
        private double rotationY = 0;
        private double rotationX = 0;
        private double rotationZ = 0;

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            Trace.beginSection("reading from gyroscope");
            float[] values = event.values;
            float x = values[0];
            float y = values[1];
            float z = values[2];

            float minSensitivity = 0.01f;

            long now = System.currentTimeMillis();
            float timeDiff = (now - lastTime) / 1000f;
            lastTime = now;
            if (timeDiff > 1) {
                timeDiff = MIN_TIME_STEP;
            }

            rotationX = x * timeDiff;
            if (Math.abs(rotationX) < minSensitivity)
                rotationX = 0;

            rotationY = y * timeDiff;
            if (Math.abs(rotationY) < minSensitivity)
                rotationY = 0;

            rotationZ = z * timeDiff;
            if (Math.abs(rotationZ) < minSensitivity)
                rotationZ = 0;

            currentAngleX = (currentAngleX + rotationX * RADIANS_TO_DEGREES) % 360;
            currentAngleY = (currentAngleY + rotationY * RADIANS_TO_DEGREES) % 360;
            currentAngleZ = (currentAngleZ + rotationZ * RADIANS_TO_DEGREES) % 360;

            currentAngleX = currentAngleX >= 0 ? currentAngleX : 360 + currentAngleX;
            currentAngleY = currentAngleY >= 0 ? currentAngleY : 360 + currentAngleY;
            currentAngleZ = currentAngleZ >= 0 ? currentAngleZ : 360 + currentAngleZ;
            Trace.endSection();

        }
    };

    private SensorEventListener gravityListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            float[] values = event.values;
            float x = values[0];
            float y = values[1];
            float z = values[2];

            gX = x;
            gY = y;
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        if (sensorGyroscope != null)
            sensorManager.registerListener(gyroscopeListener, sensorGyroscope, SensorManager.SENSOR_DELAY_NORMAL); // to do
        if (sensorGravity != null)
            sensorManager.registerListener(gravityListener, sensorGravity, SensorManager.SENSOR_DELAY_NORMAL); // to do

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                handler.postDelayed(runnable, interval);
                updateStatus();
            }
        }, interval);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (sensorGyroscope != null)
            sensorManager.unregisterListener(gyroscopeListener, sensorGyroscope);   // to do
        if (sensorGravity != null)
            sensorManager.unregisterListener(gravityListener, sensorGravity);  // to do
        handler.removeCallbacks(runnable);
    }

    public void updateStatus(){
        if (mode == 0)
            gyroscopeMode();
        else
            gravityMode();
    }

    public void gyroscopeMode(){
        ballView.moveBall(currentAngleX, currentAngleY, currentAngleZ, height, width);
        Log.d(TAG, "Angle: " + currentAngleX + ", " + currentAngleY + ", " + currentAngleZ);
        ballView.invalidate();
    }

    public void gravityMode(){
        ballView.moveByGravity(gX, gY, height, width);
        ballView.invalidate();
    }

}