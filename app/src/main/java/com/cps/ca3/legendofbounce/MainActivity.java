package com.cps.ca3.legendofbounce;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements
        View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getSupportActionBar().hide();

        Button one = (Button) findViewById(R.id.button1);
        one.setOnClickListener(this); // calling onClick() method

        Button two = (Button) findViewById(R.id.button2);
        two.setOnClickListener(this); // calling onClick() method
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                Intent intent1 = new Intent(MainActivity.this, GameActivity.class);
                Bundle b1 = new Bundle();
                b1.putInt("mode", 0); //Your id
                intent1.putExtras(b1); //Put your id to your next Intent
                startActivity(intent1);
                break;
            case R.id.button2:
                Intent intent2 = new Intent(MainActivity.this, GameActivity.class);
                Bundle b2 = new Bundle();
                b2.putInt("mode", 1); //Your id
                intent2.putExtras(b2); //Put your id to your next Intent
                startActivity(intent2);
                break;
            default:
                break;
        }
    }
}